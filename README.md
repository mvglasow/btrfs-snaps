## Features

* Daily, weekly and monthly snapshots for Btrfs subvolumes (the script decides, based on the current date, which type of snapshot to create)
* Snapshots are placed inside the subvolumes (optimized for file shares)
* Snapshots are created read-only, preventing after-the-fact alteration of data
* Expired snapshots are deleted (the last 7 daily, 5 weekly and 12 monthly snapshots are kept)
* By default, snapshots of all top-level subvolumes are created (alternatively, an explicit list can be supplied as a list of arguments)
* The above behaviors can be changed by modifying the script

## Usage

Recommended usage is to create a cron job to run this script once a day. The command to run is:

    cd /mnt/btrfs_pool1 && btrfs-snaps | grep -Ev "(Create a snapshot of|Will delete the oldest|Delete subvolume|Making snapshot of )"
    
Replace `/mnt/btrfs_pool1` with the mount point of your root volume. If `btrfs-snaps` is not on your `PATH`, specify the full path to the script.

By default, this script will snapshot all top-level subvolumes but not their children or anything underneath them. You can change this behavior by explicitly specifying a list of all subvolumes to snapshot as command line arguments.

Ensure that each subvolume you want to snapshot has either a top-level directory or subvolume called `.snapshot`. If the dir/subvolume does not exist, no snapshots will be created! A subvolume is recommended for this purpose for two major reasons:

* it is less prone to accidental deletion (`rm -r`) than a simple dir
* if `.snapshot` is a directory, each snapshot will contain an empty subdirectory for each previous snapshot, which may be undesirable and increases the overhead for a snapshot (albeit slightly).

The directory hierarchy used by this script is optimized for file servers on which each share is a subvolume in itself. That way, users will be able to access snapshots of the share by simply going to the `.snapshots` directory and choosing the snapshot they wish to use.

## Caveats

If you intend to create snapshots of a volume, it is strongly recommended to mount it with `noatime`. This will leave the “last accessed” timestamp unchanged if the file is merely read without actually changing it, and minimize the snapshot overhead as metadata does not need to be duplicated unless the file has indeed been written to.

Deletion of expired snapshots is implemented as a number of snapshots of the specified type to keep, not as a retention period. This has to major consequences:

* As daily snapshots are skipped if they coincide with weekly or monthly ones (and weekly snapshots are skipped if they coincide with monthly ones), the oldest snapshots of these type may be older than expected. The same goes if one or more daily runs are skipped or fail to create a snapshot for whatever reason.
* If this script runs more than once on a given day and against the same set of subvolumes, this will shorten the retention period for the snapshot type due on that day, by one cycle for each extra run of the script.

Such anomalies caused by failed, skipped or extra runs of the script will eventually age out of the history. Keep these limitations in mind, though, when planning your setup.